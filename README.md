# Desafio Ordem de Produção - Guararapes RCHLO

Projeto com finalidade de demonstração de conhecimento em desenvolvimento.

Técnologias/Frameworks utilizados:
 - spring-boot
 - spring-boot-security
 - JPA/Hibernate
 - Java 8
 - Maven
 - RESTful
 - lombok
 - Banco de dados em H2.
 - TypeScript
 - TypeScript
 - Ionic/Angular

# BUILD PROJETO (back-end)
- Baixar IDE https://spring.io/tools
- Baixar jar do lombok https://projectlombok.org/download, executar o comando para executar o jar: java -jar lombok.jar e seguir as indicações para encontrar o "eclipse.ini" instalado (ou spring tools IDE .ini
- Realizar o clone do projeto;
- Abrir Spring Tools IDE, importar o projeto maven e seleccionar o projeto localizado em java-skeleton. 
- Executar a classe principal: DesafioOrdemProducaoApplication.java (nesta classe, existe um pré-load de dados a serem utilizados para fins de testes).
- Verificar/Alterar código conforme necessidade. 

# BUILD PROJETO (front-end)
- Abrir o terminal na pasta do projeto localizado em angular-skeleton
- Instalar o Ionic (3) / cordova (3) através do comando: npm install -g ionic@3.19.0 cordova@7.1.0
- Executar o comando: npm install
- Executar o comando: ionic cordova run browser
