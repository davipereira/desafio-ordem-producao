import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MaterialService } from '../../service/domain/material.service';
import { MaterialDTO } from '../../models/material.dto';
import { OrdemService } from '../../service/domain/ordem.service';
import { OrdemDTO } from '../../models/ordem.dto';

@IonicPage()
@Component({
  selector: 'page-produto',
  templateUrl: 'produto.html'
})
export class ProdutoPage {

  items: MaterialDTO[];
  material: MaterialDTO;
  
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public materialService: MaterialService,
    public alertController: AlertController,
    public ordemService: OrdemService) {
  }

  ionViewDidLoad() {
    this.materialService.findAllFinalProducts()
      .subscribe(response =>  {
        this.items = response },
        error => { 
          if (error.status == 403) {
            this.navCtrl.setRoot('HomePage');
          } else {
            let alert = this.alertController.create({
              title: 'Erro!',
              message: 'Servidor fora do ar',
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    this.navCtrl.setRoot('HomePage');
                  }
                }
              ]
            });
            alert.present();
          }
        });
    }

    presentAlertConfirm(material : MaterialDTO) {
      this.material = material;
      const alert = this.alertController.create({
        title: 'Confirmação',
        message: '<strong>Confirma a Ordem de Produção ?</strong>',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {}
          }, {
            text: 'Confirmar',
            handler: () => {
              let ordem : OrdemDTO = {
                id: null,
                materialId: this.material.id
              }

              this.ordemService.createOrdem(ordem).subscribe(response =>  {
                  this.showInsertOk();
                }, error => { 
                  if (error.status == 403) {
                    this.navCtrl.setRoot('HomePage');
                  }
                });
            }
          }
        ]
      });
       alert.present();
    }

    showInsertOk() {
      let alert = this.alertController.create({
        title: 'Sucesso!',
        message: 'Ordem de Produção efetuada com sucesso.',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.navCtrl.setRoot('ProdutoPage');
            }
          }
        ]
      });

      alert.present();
    }
}
