import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MaterialDTO } from "../../models/material.dto";
import { Observable } from "rxjs/Rx";
import { API_CONFIG } from "../../config/api.config";

@Injectable()
export class MaterialService { 

    constructor (public http: HttpClient) { 
    }

    findAllFinalProducts() : Observable<MaterialDTO[]> {
        return this.http.get<MaterialDTO[]>(`${API_CONFIG.baseUrl}/materiais/produtosfinais/`);
    }
} 