import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { OrdemDTO } from "../../models/ordem.dto";
import { API_CONFIG } from "../../config/api.config";

@Injectable()
export class OrdemService { 

    constructor (public http: HttpClient) { 
    }

    createOrdem(ordem : OrdemDTO) {
        return this.http.post(`${API_CONFIG.baseUrl}/ordens`,
            ordem, 
            {
                observe: 'response',
                responseType: 'text'
            }
        );
    }
} 