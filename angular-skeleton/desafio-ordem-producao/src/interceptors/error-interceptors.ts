import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Observable } from "rxjs";
import { AlertController } from "ionic-angular";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(public alertControle : AlertController) {}
    
    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        return next.handle(req).catch((error) => {
            let errorObj = error;
            if (errorObj.error) {
                errorObj = errorObj.error;
            }
            if(!errorObj.status) {
                errorObj = JSON.parse(errorObj);
            }

            switch(errorObj.status) {
                case 403:
                    this.handle403();
                    break;

                default:
                    this.handleDefaulError(errorObj);
                    break;  
            }

            return Observable.throw(errorObj);
        }) as any;
    }

    handle403() {
        let alert = this.alertControle.create({
            title: 'Erro 403: Falha na autorização',
            message: 'Operação não permitida.',
            enableBackdropDismiss: false,
            buttons : [
                {
                    text: 'OK'
                }
            ]
        });
        alert.present();
    }

    handleDefaulError(errorObj) {
        let alert = this.alertControle.create({
            title: 'Erro ' + errorObj.status +': ' + errorObj.msg,
            message: errorObj.erros[0].message,
            enableBackdropDismiss: false,
            buttons : [
                {
                    text: 'OK'
                }
            ]           
        });
        alert.present();
    }
}

export const ErrorInterceptorProvider = { 
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true,
}