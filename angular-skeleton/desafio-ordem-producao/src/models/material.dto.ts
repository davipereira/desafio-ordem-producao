import { EstoqueDTO } from "../models/estoque.dto";

export interface MaterialDTO {
    id : string;
    nome : string;
    descricao : string;
    estoque : EstoqueDTO;
    temEstoqueMateriaPrima : boolean;
}