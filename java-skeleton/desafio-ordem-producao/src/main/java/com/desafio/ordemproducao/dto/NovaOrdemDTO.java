package com.desafio.ordemproducao.dto;

import javax.validation.constraints.NotNull;

import com.desafio.ordemproducao.domain.Ordem;
import com.desafio.ordemproducao.services.validation.OrdemInsert;
import com.desafio.ordemproducao.services.validation.OrdemUpdate;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * DTO de Nova Ordem de Produção
 */
@NoArgsConstructor
@AllArgsConstructor
@OrdemInsert
@OrdemUpdate
public class NovaOrdemDTO {

	@Getter
	@Setter
	@JsonIgnore
	private Integer id;
	
	@Getter
	@Setter
	@NotNull(message = "É necessário informar o número de identificação do material")
	private Integer materialId;
	
	/**
	 * Construtor Ordem
	 * @param ordem
	 */
	public NovaOrdemDTO(Ordem ordem) {
		this.id = ordem.getId();
		if (ordem.getMaterial() != null)
			this.materialId = ordem.getMaterial().getId();
	}
}
