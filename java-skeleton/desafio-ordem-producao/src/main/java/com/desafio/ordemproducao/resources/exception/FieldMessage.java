package com.desafio.ordemproducao.resources.exception;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 *
 * Objeto para encapsular e trafegar informações de erros do sistema.
 */
@NoArgsConstructor
public class FieldMessage implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	private String fieldName;
	
	@Getter
	@Setter
	private String message;
	
	public FieldMessage(String fieldName, String message) {
		this.fieldName = fieldName;
		this.message = message;
	}
}
