package com.desafio.ordemproducao.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.desafio.ordemproducao.domain.Material;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho of 2020
 * 
 * DTO para inclusão de um novo Material 
 */
@NoArgsConstructor
@AllArgsConstructor
public class NovoMaterialDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	@JsonIgnore
	private Integer id;
	
	@Getter
	@Setter
	@NotEmpty(message = "É necessário informar o nome do material")
	private String nome;
	
	@Getter
	@Setter
	private String descricao;
	
	@Getter
	@Setter
	private Integer quantidadeEstoque;
	
	@Getter
	@Setter
	private Integer idMaterialSuperior;
	
	/**
	 * Construtor MaterialDTO
	 * @param ordem
	 */
	public NovoMaterialDTO(Material material) {
		this.id = material.getId();
		this.nome = material.getNome();
		this.descricao = material.getDescricao();
	}
}
