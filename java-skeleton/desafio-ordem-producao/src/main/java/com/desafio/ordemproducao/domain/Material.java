package com.desafio.ordemproducao.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho of 2020
 * 
 * Entidade de Material
 */
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class Material implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	@Getter
	@Setter
	private Integer id;
	
	@Getter
	@Setter
	@Column(nullable = false)
	private String nome;
	
	@Getter
	@Setter
	private String descricao;
	
	@Getter
	@Setter
	@OneToOne(cascade=CascadeType.ALL, mappedBy = "material")
	private Estoque estoque;
	
	@Getter
	@Setter
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "material")
	private Set<Ordem> ordens = new HashSet<Ordem>();
	
	@Getter
	@Setter
	@OneToMany(mappedBy = "materialSuperior", fetch = FetchType.EAGER)
	@JsonIgnore
	private List<Material> materiais = new ArrayList<Material>();
	
	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "material_id")
	private Material materialSuperior;
	
	@Getter
	@Setter
	@Transient
	private Boolean temEstoqueMateriaPrima;

	/**
	 * Construtor de Material 
	 * @param id
	 * @param nome
	 * @param descricao
	 * @param estoque
	 * @param produtoFinal
	 */
	public Material(Integer id, @NotNull @NotEmpty String nome, String descricao, Estoque estoque,
			Material materialSuperior) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.estoque = estoque;
		this.materialSuperior = materialSuperior;
	}
}
