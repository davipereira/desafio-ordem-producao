package com.desafio.ordemproducao.resources.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.desafio.ordemproducao.services.exceptions.DataIntegrityException;
import com.desafio.ordemproducao.services.exceptions.ObjectNotFoundException;

/**
 * @author Davi Pereira <pereiradavi@gmail.com>
 * @since Julho de 2020
 * 
 * Captura e trata erros do sistema. Especificamente erros entre as camadas model e controller.
 */
@ControllerAdvice
public class ResourceExceptionHandler {

	/**
	 * Processa e trata o erro de objeto não encontrado.
	 * @param ObjectNotFoundException
	 * @param HttpServletRequest
	 * 
	 * @return ResponseEntity<StandardError>
	 */
	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<StandardError> objectNotFound (ObjectNotFoundException e, HttpServletRequest request) {
		
		StandardError err = new StandardError(HttpStatus.NOT_FOUND.value(), e.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
	}
	
	/**
	 * Processa e trata erro de integridade com o banco de dados.
	 * @param DataIntegrityException
	 * @param HttpServletRequest
	 * 
	 * @return ResponseEntity<StandardError>
	 */
	@ExceptionHandler(DataIntegrityException.class)
	public ResponseEntity<StandardError> dataIntegrity (DataIntegrityException e, HttpServletRequest request) {
		
		StandardError err = new StandardError(HttpStatus.NOT_FOUND.value(), e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}
	
	/**
	 * Processa erros de validação 
	 * @param MethodArgumentNotValidException
	 * @param HttpServletRequest
	 * 
	 * @return ResponseEntity<StandardError>
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<StandardError> validation (MethodArgumentNotValidException e, HttpServletRequest request) {
		
		ValidationError validationError = new ValidationError(
				HttpStatus.NOT_FOUND.value(), "Erro de Validação");
		
		for (FieldError x : e.getBindingResult().getFieldErrors()) {
			validationError.addError(x.getField(), x.getDefaultMessage());
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(validationError);
	}
	
}
