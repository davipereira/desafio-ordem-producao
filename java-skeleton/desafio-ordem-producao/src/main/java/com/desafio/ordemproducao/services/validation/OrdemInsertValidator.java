package com.desafio.ordemproducao.services.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import com.desafio.ordemproducao.domain.Material;
import com.desafio.ordemproducao.dto.NovaOrdemDTO;
import com.desafio.ordemproducao.resources.exception.FieldMessage;
import com.desafio.ordemproducao.services.MaterialService;

public class OrdemInsertValidator implements ConstraintValidator<OrdemInsert, NovaOrdemDTO> {
	
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private MaterialService materialService;
	
	@Override
	public void initialize(OrdemInsert ann) {
	}

	@Override
	public boolean isValid(NovaOrdemDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		@SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		String idRequest = map.get("id");
		
		if (idRequest == null) {
			try {
				Material material = materialService.findById(objDto.getMaterialId());
				
				if (material.getMaterialSuperior() != null) 
					list.add(new FieldMessage("materialId", "Operação inválida! O Material informado não é considerado um Produto Final."));
				
				if (!materialService.verificaEstoqueMateriaPrima(material))
					list.add(new FieldMessage("materialId", "O Material " +material.getNome() + " não possui matéria prima suficiente"));
			} catch (ObjectNotFoundException e) {
				list.add(new FieldMessage("materialId", e.getMessage()));
			}
			
			for (FieldMessage e : list) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
						.addConstraintViolation();
			}
		}
		
		return list.isEmpty();
	}
}
