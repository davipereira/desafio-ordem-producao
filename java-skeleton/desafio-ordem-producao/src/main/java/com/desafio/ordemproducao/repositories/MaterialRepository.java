package com.desafio.ordemproducao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.desafio.ordemproducao.domain.Material;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since July of 2020
 * 
 * Auxiliar para tratamento de informações de Material na Base de Dados.
 */
@Repository
public interface MaterialRepository extends JpaRepository<Material, Integer> {

	/**
	 * Busca Materiais designados como produtos finais.
	 * Um Material é considerado produto final quando não possui material pai. 
	 * 
	 * @param id
	 * @return List<Material>
	 */
	@Transactional(readOnly = true)
	List<Material> findByMaterialSuperiorIsNull();
}
