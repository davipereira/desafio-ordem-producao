package com.desafio.ordemproducao.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.desafio.ordemproducao.domain.Material;
import com.desafio.ordemproducao.dto.MaterialDTO;
import com.desafio.ordemproducao.dto.MaterialProdutoFinalDTO;
import com.desafio.ordemproducao.dto.NovoMaterialDTO;
import com.desafio.ordemproducao.services.MaterialService;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 *
 * API de Materiais
 */
@RestController
@RequestMapping(value = "/materiais")
public class MaterialResource {

	@Autowired
	private MaterialService service;
	
	/**
	 * Busca e retorna todos os materiais cadastrados no sistema. 
	 * 
	 * @return ResponseEntity<<List<MaterialDTO>> - Lista de Materiais
	 */
	@RequestMapping (method = RequestMethod.GET)
	public ResponseEntity<List<MaterialDTO>> findAll(){
		List<Material> listaMaterial = service.findAll();
		List<MaterialDTO> listaDTO 
			= listaMaterial.stream().map(obj -> new MaterialDTO(obj)).collect(Collectors.toList());
	
		return ResponseEntity.ok().body(listaDTO);
	}
	
	/**
	 * Busca e retorna um Material específico, dado o parâmetro de id recebido. 
	 * 
	 * @param id
	 * @return ResponseEntity<MaterialDTO>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<MaterialDTO> findById(@PathVariable Integer id) {
		
		Material user = service.findById(id);
		MaterialDTO objDto = new MaterialDTO(user);
		return ResponseEntity.ok().body(objDto);
	}
	
	/**
	 * Busca e retorna todos os elegíveis a produto final. 
	 * 
	 * @return ResponseEntity<List<MaterialProdutoFinalDTO>> - Lista de Materiais considerados produtos finais
	 */
	@RequestMapping (value = "/produtosfinais", method = RequestMethod.GET)
	public ResponseEntity<List<MaterialProdutoFinalDTO>> findAllProdutoFinal(){
		List<Material> listaMaterial = service.findAllFinalProducts();
		List<MaterialProdutoFinalDTO> listaDTO 
			= listaMaterial.stream().map(obj -> new MaterialProdutoFinalDTO(obj)).collect(Collectors.toList());
	
		return ResponseEntity.ok().body(listaDTO);
	} 
	
	/**
	 * Insere um novo Material
	 * 
	 * @param MaterialDTO
	 * @return ResponseEntity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody NovoMaterialDTO objDTO) {
		objDTO = new NovoMaterialDTO(service.insert(service.fromDTO(objDTO)));
		
		URI url = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(objDTO.getId()).toUri();
		
		return ResponseEntity.created(url).build();
	}
	
	/**
	 * Atualiza um Material especifico.
	 * 
	 * @param objDTO
	 * @param id
	 * @return ResponseEntity
	 */
	@RequestMapping(value ="/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody NovoMaterialDTO objDTO, @PathVariable Integer id) {
		objDTO.setId(id);
		objDTO = new NovoMaterialDTO(service.update(service.fromDTO(objDTO)));
		
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Apaga o Meterial informado através do parâmetro de identificação.
	 * @param id
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
}
