package com.desafio.ordemproducao.dto;

import java.io.Serializable;

import com.desafio.ordemproducao.domain.Material;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho of 2020
 * 
 * DTO de um Material Superior 
 */
public class MaterialSuperiorDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	private Integer id;
	
	@Getter
	@Setter
	private String nome;
	
	@Getter
	@Setter
	private String descricao;
	
	/**
	 * Construtor de MaterialSuperiorDTO 
	 * 
	 * @param id
	 * @param nome
	 * @param descricao
	 */
	public MaterialSuperiorDTO(Integer id, String nome, String descricao) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public MaterialSuperiorDTO(Material material) {
		this.id = material.getId();
		this.nome = material.getNome();
		this.descricao = material.getDescricao();
	}
}
