package com.desafio.ordemproducao.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * DTO de Estoque
 */
@NoArgsConstructor
@AllArgsConstructor
public class EstoqueDTO {

	@Getter
	@Setter
	private Integer id;
	
	@Getter
	@Setter
	private Integer quantidade;
	
}
