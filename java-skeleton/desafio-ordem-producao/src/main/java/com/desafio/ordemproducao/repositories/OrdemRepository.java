package com.desafio.ordemproducao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.desafio.ordemproducao.domain.Material;
import com.desafio.ordemproducao.domain.Ordem;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since July of 2020
 * 
 * Auxiliar para tratamento de informações de Ordens na Base de Dados.
 */
@Repository
public interface OrdemRepository extends JpaRepository<Ordem, Integer> {

	/**
	 * Busca Ordens dado o material id informado
	 * Um Material é considerado produto final quando não possui material pai. 
	 * 
	 * @param id
	 * @return List<Ordem>
	 */
	@Transactional(readOnly = true)
	List<Ordem> findByMaterial(Material material);
}
