package com.desafio.ordemproducao.dto;

import com.desafio.ordemproducao.domain.Ordem;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * DTO de Ordem de Produção
 */
@NoArgsConstructor
@AllArgsConstructor
public class OrdemDTO {

	@Getter
	@Setter
	private Integer id;
	
	@Getter
	@Setter
	private MaterialDTO material;
	
	/**
	 * Construtor Ordem
	 * @param material
	 */
	public OrdemDTO(Ordem ordem) {
		this.id = ordem.getId();
		if (ordem.getMaterial() != null)
			this.material = new MaterialDTO(ordem.getMaterial());
	}
}
