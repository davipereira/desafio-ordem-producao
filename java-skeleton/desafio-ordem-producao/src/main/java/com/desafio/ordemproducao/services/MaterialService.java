package com.desafio.ordemproducao.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.desafio.ordemproducao.domain.Estoque;
import com.desafio.ordemproducao.domain.Material;
import com.desafio.ordemproducao.dto.NovoMaterialDTO;
import com.desafio.ordemproducao.repositories.MaterialRepository;
import com.desafio.ordemproducao.services.exceptions.DataIntegrityException;
import com.desafio.ordemproducao.services.exceptions.ObjectNotFoundException;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * Service de Material para Tratamento de regras de negócio que envolve um Material
 */
@Service
public class MaterialService {

	@Autowired
	private MaterialRepository materialRepository;
	@Autowired
	private EstoqueService estoqueService;
	
	/**
	 * Recupera todos os materiais.
	 * 
	 * @return Lista de Materiais.
	 */
	public List<Material> findAll() {
		return materialRepository.findAll();
	}
	
	/**
	 * Recupera todos os materiais considerados produtos finais.
	 * @return Lista de Materiais considerados produtos finais.
	 */
	public List<Material> findAllFinalProducts() {
		List<Material> listaMaterialProdutoFinal = materialRepository.findByMaterialSuperiorIsNull();
		for (Material material : listaMaterialProdutoFinal) {
			material.setTemEstoqueMateriaPrima(verificaEstoqueMateriaPrima(material));
		}
		
		return materialRepository.findByMaterialSuperiorIsNull();
	}

	/**
	 * Busca uma Material dado o correspondente número de identificação. 
	 * 
	 * @param id
	 * @return Material
	 * @throws ObjectNotFoundException
	 */
	public Material findById(Integer id) {
		Optional<Material> obj = materialRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Material não encontrado! Id: " + id));
	}

	/**
	 * Salva um material 
	 * 
	 * @param Material
	 * @return Material
	 */
	public Material insert(Material material) {
		material.setId(null);
		return materialRepository.save(material);
	}

	/**
	 * Constructs a Material object.
	 * 
	 * @param MaterialDTO
	 * @return Material
	 */
	public Material fromDTO(NovoMaterialDTO objDTO) {
		Material materialSuperior = null;
		if (objDTO.getIdMaterialSuperior() != null) {
			materialSuperior = this.findById(objDTO.getIdMaterialSuperior());
		}
		Integer qtdEstoque = objDTO.getQuantidadeEstoque() != null ? objDTO.getQuantidadeEstoque() : 0;
		Estoque estoque = new Estoque(qtdEstoque, null);
		
		Material material = new Material(objDTO.getId(), objDTO.getNome(), objDTO.getDescricao(), 
				estoque, materialSuperior); 
		estoque.setMaterial(material);

		return material;
	}
	
	/**
	 * Atualiza um Material
	 * 
	 * @param Material
	 * @return Material
	 */
	public Material update(Material material) {
		Material newObj = this.findById(material.getId());
		
		this.updateDate(newObj, material);

		Estoque estoque = estoqueService.findById(material.getId());
		estoque.setQuantidade(material.getEstoque().getQuantidade() );
		newObj.setEstoque(estoque);
		
		return this.materialRepository.save(newObj);
	}

	/**
	 * Apaga um material dado o id informado.
	 * @param id
	 */
	public void delete(Integer id) {
		this.findById(id);
		
		try {
			this.materialRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("É Necessário apagar as materias primas primeiro.");
		}
	}

	/**
	 *  Atualiza o objeto do banco de dados com as alterações recebidas para alteração do Material.
	 * 
	 * @param newObj
	 * @param obj
	 */
	private void updateDate(Material newObj, Material obj) {
		newObj.setDescricao(obj.getDescricao());
		newObj.setMaterialSuperior(obj.getMaterialSuperior());
		newObj.setNome(obj.getNome());
	}
	
	/**
	 * Verifica se todas as materias primas do material passado como parâmetro possui estoque. 
	 * @param material
	 * @return true se possuir, false se não possuir.
	 */
	public boolean verificaEstoqueMateriaPrima(Material material) {
		for (Material materiaPrima : material.getMateriais()) {
			if (materiaPrima.getEstoque() == null ||  materiaPrima.getEstoque().getQuantidade() == null
					|| materiaPrima.getEstoque().getQuantidade() <= 0) {
				
				return Boolean.FALSE;
			} else {
				for (Material matPrima : material.getMateriais()) {
					return this.verificaEstoqueMateriaPrima(matPrima);
				}
			}
		}
		 return Boolean.TRUE;
	}
	
	/**
	 * Consome 1 item de estoque de cada materia prima do material considerado produto final.
	 * @param material
	 */
	public void consomeItemEstoqueMateriaPrima(Material material) {
		for (Material materiaPrima : material.getMateriais()) {
			if (materiaPrima.getEstoque() != null ||  materiaPrima.getEstoque().getQuantidade() != null) {
				
				int novaQtd = materiaPrima.getEstoque().getQuantidade().intValue() - 1;
				materiaPrima.getEstoque().setQuantidade(novaQtd);
				
				this.update(materiaPrima);
				this.consomeItemEstoqueMateriaPrima(materiaPrima);
			} 
		}
	}
}
