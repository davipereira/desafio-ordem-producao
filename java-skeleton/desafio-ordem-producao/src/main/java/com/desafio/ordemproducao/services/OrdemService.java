package com.desafio.ordemproducao.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.desafio.ordemproducao.domain.Material;
import com.desafio.ordemproducao.domain.Ordem;
import com.desafio.ordemproducao.dto.NovaOrdemDTO;
import com.desafio.ordemproducao.repositories.OrdemRepository;
import com.desafio.ordemproducao.services.exceptions.DataIntegrityException;
import com.desafio.ordemproducao.services.exceptions.ObjectNotFoundException;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * Service de Ordem para Tratamento de regras de negócio que envolve uma Ordem de Produção
 */
@Service
public class OrdemService {

	@Autowired
	private OrdemRepository ordemRepository;
	@Autowired 
	private MaterialService materialService;
	
	/**
	 * Recupera todas as ordens.
	 * 
	 * @return Lista de Ordem.
	 */
	public List<Ordem> findAll() {
		return ordemRepository.findAll();
	}
	
	/**
	 * Recupera todas as ordens dado o material id informado.
	 * @return List<Ordem> - Lista de Ordens
	 */
	public List<Ordem> findAllOrdersByMaterial(Integer materialId) {
		Material material = materialService.findById(materialId);
		return ordemRepository.findByMaterial(material);
	}

	/**
	 * Busca uma Ordem dado o correspondente número de identificação. 
	 * 
	 * @param id
	 * @return Ordem
	 * @throws ObjectNotFoundException
	 */
	public Ordem findById(Integer id) {
		Optional<Ordem> obj = ordemRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Ordem não encontrada! Id: " + id));
	}

	/**
	 * Salva uma ordem 
	 * 
	 * @param Ordem
	 * @return Ordem
	 */
	public Ordem insert(Ordem ordem) {
		ordem.setId(null);
		
		materialService.consomeItemEstoqueMateriaPrima(ordem.getMaterial());
		return ordemRepository.save(ordem);
	}

	/**
	 * Construtor OrdemDTO object.
	 * 
	 * @param OrdemDTO
	 * @return Ordem
	 */
	public Ordem fromDTO(NovaOrdemDTO objDTO) {
		Material material = null;
		material = materialService.findById(objDTO.getMaterialId());
		
		Ordem ordem = new Ordem(objDTO.getId(), material);
		return ordem;
	}
	
	/**
	 * Atualiza uma Ordem
	 * 
	 * @param Ordem
	 * @return Ordem - Ordem atualizada
	 */
	public Ordem update(Ordem ordem) {
		Ordem newObj = this.findById(ordem.getId());
		newObj.setMaterial(ordem.getMaterial());
		
		return this.ordemRepository.save(newObj);
	}

	/**
	 * Deleta uma ordem dado o id informado. 
	 * @param id
	 */
	public void delete(Integer id) {
		this.findById(id);
		
		try {
			this.ordemRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("É necessário apagar o material referente a "
					+ "ordem antes de prosseguir com esta operação.");
		}
	}

}
