package com.desafio.ordemproducao.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * Entidade de Controle de Estoque
 */
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class Estoque {

	@Id
	@EqualsAndHashCode.Include
	@Getter
	@Setter
	private Integer id;
	
	@Getter
	@Setter
	private Integer quantidade;
	
	@Getter
	@Setter
	@OneToOne
	@JoinColumn(name = "material_id", unique = true)
	@MapsId
	@JsonIgnore
	private Material material;
	
	public Estoque(Integer quantidade, Material material) {
		this.quantidade = quantidade;
		this.material = material;
	}
	
	
	
}
