package com.desafio.ordemproducao;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.desafio.ordemproducao.domain.Estoque;
import com.desafio.ordemproducao.domain.Material;
import com.desafio.ordemproducao.domain.Ordem;
import com.desafio.ordemproducao.repositories.MaterialRepository;
import com.desafio.ordemproducao.repositories.OrdemRepository;

@SpringBootApplication
public class DesafioOrdemProducaoApplication implements CommandLineRunner {

	@Autowired
	private MaterialRepository materialRepository;
	
	@Autowired
	private OrdemRepository ordemRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(DesafioOrdemProducaoApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		Material mat1 = new Material(null, "T-Shirt Azul do Mar", "Camiseta 2021", null, null);
		Material mat2 = new Material(null, "Tecido Azul", "Tecido Azul", null, mat1);
		
		Material mat3 = new Material(null, "Tinta Azul", "Tinta Azul", null, mat2);
		Material mat4 = new Material(null, "Linha Branca", "Linha Branca", null, mat2);
		Material mat5 = new Material(null, "Tonalizante", "Tonalizante", null, mat3);
		
		Material mat6 = new Material(null, "Bermuda Like 2021", "Bermuda 2021", null, null);
		Material mat7 = new Material(null, "Cinto Branco", "Cinto Branco", null, mat6);
		
		Estoque est1 = new Estoque(0, mat1);
		Estoque est2 = new Estoque(2, mat2);
		Estoque est3 = new Estoque(2, mat3);
		Estoque est4 = new Estoque(2, mat4);
		Estoque est5 = new Estoque(1, mat5);
		Estoque est6 = new Estoque(0, mat6);
		Estoque est7 = new Estoque(2, mat7);
		
		mat1.setEstoque(est1);
		mat2.setEstoque(est2);
		mat3.setEstoque(est3);
		mat4.setEstoque(est4);
		mat5.setEstoque(est5);
		mat6.setEstoque(est6);
		mat7.setEstoque(est7);
		
		materialRepository.saveAll(Arrays.asList(mat1, mat2, mat3, mat4, mat5, mat6, mat7));
		
		Ordem ord1 = new Ordem(null, mat1);
		Ordem ord2 = new Ordem(null, mat6);
		ordemRepository.saveAll(Arrays.asList(ord1, ord2));
	}  

}
