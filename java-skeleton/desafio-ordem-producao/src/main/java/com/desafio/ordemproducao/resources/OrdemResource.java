package com.desafio.ordemproducao.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.desafio.ordemproducao.domain.Ordem;
import com.desafio.ordemproducao.dto.NovaOrdemDTO;
import com.desafio.ordemproducao.dto.OrdemDTO;
import com.desafio.ordemproducao.services.OrdemService;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 *
 * API Ordem
 */
@RestController
@RequestMapping(value = "/ordens")
public class OrdemResource {

	@Autowired
	private OrdemService service;
	
	/**
	 * Busca e retorna todas ordens cadastrados no sistema. 
	 * 
	 * @return ResponseEntity<<List<OrdemDTO>> - Lista de Ordem
	 */
	@RequestMapping (method = RequestMethod.GET)
	public ResponseEntity<List<OrdemDTO>> findAll(){
		List<Ordem> listaOrdem = service.findAll();
		List<OrdemDTO> listaDTO 
			= listaOrdem.stream().map(obj -> new OrdemDTO(obj)).collect(Collectors.toList());
	
		return ResponseEntity.ok().body(listaDTO);
	}
	
	/**
	 * Busca e retorna uma Ordem específica, dado o parâmetro de id recebido. 
	 * 
	 * @param id
	 * @return ResponseEntity<OrdemDTO>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<OrdemDTO> find(@PathVariable Integer id) {
		
		Ordem user = service.findById(id);
		OrdemDTO objDto = new OrdemDTO(user);
		return ResponseEntity.ok().body(objDto);
	}
	
	/**
	 * Busca e retorna todos as ordens do material informado. 
	 * 
	 * @return ResponseEntity<List<OrdemDTO>> - Lista de Ordens
	 */
	@RequestMapping (value = "/ordenspormaterial/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<OrdemDTO>> findAllOrdersByMaterial(@PathVariable Integer id){
		List<Ordem> listaOrdem = service.findAllOrdersByMaterial(id);
		List<OrdemDTO> listaDTO 
			= listaOrdem.stream().map(obj -> new OrdemDTO(obj)).collect(Collectors.toList());
	
		return ResponseEntity.ok().body(listaDTO);
	} 
	
	/**
	 * Insere um novo Material
	 * 
	 * @param OrdemDTO
	 * @return ResponseEntity
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody NovaOrdemDTO objDTO) {
		objDTO = new NovaOrdemDTO(service.insert(service.fromDTO(objDTO)));
		
		URI url = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(objDTO.getId()).toUri();
		
		return ResponseEntity.created(url).build();
	}
	
	/**
	 * Atualiza uma Ordem especifica.
	 * 
	 * @param NovaOrdemDTO - objDTO
	 * @param id
	 * @return ResponseEntity
	 */
	@RequestMapping(value ="/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody NovaOrdemDTO objDTO, @PathVariable Integer id) {
		objDTO.setId(id);
		objDTO = new NovaOrdemDTO(service.update(service.fromDTO(objDTO)));
		
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Apaga a ordem informada através do parâmetro de identificação.
	 * @param id
	 * @return ResponseEntity
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
}
