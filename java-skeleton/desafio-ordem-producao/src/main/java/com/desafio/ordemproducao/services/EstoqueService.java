package com.desafio.ordemproducao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.ordemproducao.domain.Estoque;
import com.desafio.ordemproducao.repositories.EstoqueRepository;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * Service de Estoque para Tratamento de regras de negócio que o envolve
 */
@Service
public class EstoqueService {

	@Autowired
	private EstoqueRepository estoqueRepository;
	
	/**
	 * Busca o estoque dado o seu correspondente número de identificação. 
	 * 
	 * @param id
	 * @return Estoque
	 */
	public Estoque findById(Integer id) {
		Optional<Estoque> obj = estoqueRepository.findById(id);
		return obj.orElse(null);
	}
	
}
