package com.desafio.ordemproducao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desafio.ordemproducao.domain.Estoque;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since July of 2020
 * 
 * Auxiliar para tratamento de informações de Estoque na Base de Dados.
 */
@Repository
public interface EstoqueRepository extends JpaRepository<Estoque, Integer> {

}
