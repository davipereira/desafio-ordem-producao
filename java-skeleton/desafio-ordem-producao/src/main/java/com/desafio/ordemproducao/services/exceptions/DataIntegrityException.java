package com.desafio.ordemproducao.services.exceptions;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * Exception para erro de integridade com o banco de dados.
 */
public class DataIntegrityException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public DataIntegrityException(String args) {
		super(args);
	}

	public DataIntegrityException(String args, Throwable cause) {
		super (args, cause);
	}
}
