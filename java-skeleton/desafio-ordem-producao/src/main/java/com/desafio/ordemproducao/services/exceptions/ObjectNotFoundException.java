package com.desafio.ordemproducao.services.exceptions;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho de 2020
 * 
 * Exception para erro de Objeto não encontrado com o banco de dados.
 */
public class ObjectNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public ObjectNotFoundException(String args) {
		super(args);
	}

	public ObjectNotFoundException(String args, Throwable cause) {
		super (args, cause);
	}
}
