package com.desafio.ordemproducao.dto;

import java.io.Serializable;

import com.desafio.ordemproducao.domain.Material;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Davi Pereira <pereiradavipe@gmail.com>
 * @since Julho of 2020
 * 
 * DTO de um material
 */
public class MaterialDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	private Integer id;
	
	@Getter
	@Setter
	private String nome;
	
	@Getter
	@Setter
	private String descricao;
	
	@Getter
	@Setter
	private EstoqueDTO estoque;
	
	@Getter
	@Setter
	private MaterialSuperiorDTO materialSuperior;
	
	/**
	 * Construtor de MaterialDTO 
	 * 
	 * @param id
	 * @param nome
	 * @param descricao
	 * @param estoque
	 * @param produtoFinal
	 */
	public MaterialDTO(Integer id, String nome, String descricao, EstoqueDTO estoque,
			MaterialSuperiorDTO materialSuperior, Boolean temEstoqueMateriaPrima) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.estoque = estoque;
		this.materialSuperior = materialSuperior;
	}
	
	public MaterialDTO(Material material) {
		this.id = material.getId();
		this.nome = material.getNome();
		this.descricao = material.getDescricao();
		
		this.estoque = material.getEstoque() != null 
				? new EstoqueDTO(material.getEstoque().getId(), material.getEstoque().getQuantidade()) : null;
		
		this.materialSuperior = material.getMaterialSuperior() != null
				? new MaterialSuperiorDTO(material.getMaterialSuperior()) : null;
	}
}
